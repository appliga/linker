<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/test")
 */
class TestController extends AbstractController
{
    private const CLIMACELL_KEY = 'gCpGTaF8PNT51IspKQMcbzNs6TxeBcIZ';

    /**
     * @Route("/fire-index", name="fire_index_test", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $client = new \GuzzleHttp\Client();

        $lat = '52.76627080000001';
        $lon = '15.266428800000002';

        $query = "lat=$lat&lon=$lon&apikey=".self::CLIMACELL_KEY;
        $response = $client->request('GET', 'https://api.climacell.co/v3/insights/fire-index?'.$query);
        $avgFireIndex = json_decode($response->getBody()->getContents(), true)[0];

        $query = "lat=$lat&lon=$lon&fields=fire_index&apikey=".self::CLIMACELL_KEY;
        $response = $client->request('GET', 'https://api.climacell.co/v3/weather/realtime?'.$query);
        $fireIndex = json_decode($response->getBody()->getContents(), true);

        $fires = $this->fires();
        $fires2 = $this->fires2();

        return $this->render('test/index.html.twig', [
            'lat' => $lat,
            'lon' => $lon,
            'avgFireIndex' => $avgFireIndex ?? null,
            'fireIndex' => $fireIndex ?? null,
            'fires' => array_merge($fires, $fires2),
        ]);
    }

    public function fires()
    {
        $response = file_get_contents('https://api.aerisapi.com/fires/Milton, florida?&format=json&client_id=auAvSdqF9giZ6tv51pb0d&client_secret=VufnIvpgZkeTTxL0TVqKvqqGiHzYKaLnR77hhoPE');
        $json = json_decode($response, true);
        if (true == $json['success']) {
            // create reference to our returned observation object
            return $json['response'];
        } else {
            return [];
        }
    }

    public function fires2()
    {
        // fetch Aeris API output as a string and decode into an object
        $response = file_get_contents('https://api.aerisapi.com/fires/doral, florida?&format=json&client_id=auAvSdqF9giZ6tv51pb0d&client_secret=VufnIvpgZkeTTxL0TVqKvqqGiHzYKaLnR77hhoPE');
        $json = json_decode($response, true);
        if (true == $json['success']) {
            // create reference to our returned observation object
            return $json['response'];
        } else {
        }
    }
}
