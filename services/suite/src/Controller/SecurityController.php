<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use App\Repository\UserRepository;
use App\Security\PasswordUpdater;
use Ramsey\Uuid\Uuid;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('dashboard');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/forgot-password", name="forgot_password")
     */
    public function forgotPassword(
        AuthenticationUtils $authenticationUtils,
        Request $request,
        UserRepository $userRepository,
        \Swift_Mailer $mailer,
        UrlHelper $urlHelper
    ): Response {
        $error = $authenticationUtils->getLastAuthenticationError();

        if ($request->isMethod('POST')) {
            $email = $request->request->get('email');
            $user = $userRepository->findOneByEmail($email);

            if ($user) {
                $em = $this->getDoctrine()->getManager();

                $token = Uuid::uuid4().Uuid::uuid4();

                $user->setResetToken($token);
                $user->setResetTokenExpirationDate((new \DateTime('now'))->add(new \DateInterval('P1D')));

                $em->persist($user);
                $em->flush();

                $resetLink = $urlHelper->getAbsoluteUrl($this->generateUrl('reset_password')).'?token='.$token;

                $message = (new Swift_Message('Firexo - Reset Password'))
                    ->setFrom('itsupport@firexo.com', 'Firexo IT Team')
                    ->setTo($email)
                    ->setBody(
                        $this->renderView(
                            'security/reset-password-email.html.twig',
                            ['resetLink' => $resetLink]
                        ),
                    )
                ;

                $mailer->send($message);

                $this->addFlash(
                    'success',
                    'Email with password reset details has been sent!'
                );

                return $this->redirectToRoute('forgot_password');
            }

            $this->addFlash(
                'danger',
                'We can\'t find user by this email'
            );

            return $this->redirectToRoute('forgot_password');
        }

        return $this->render('security/forgot-password.html.twig', [
            'error' => $error,
        ]);
    }

    /**
     * @Route("/reset-password", name="reset_password")
     */
    public function resetPassword(
        AuthenticationUtils $authenticationUtils,
        Request $request,
        UserRepository $userRepository,
        PasswordUpdater $passwordUpdater
    ): Response {
        $error = $authenticationUtils->getLastAuthenticationError();

        $resetToken = $request->query->get('token');
        $user = $userRepository->findOneByResetToken($resetToken);

        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user);
        $changePasswordForm->handleRequest($request);

        if ($request->query->has('token') && (!$user || ($user && $user->getResetTokenExpirationDate() < new \DateTime('now')))) {
            return $this->redirect($this->generateUrl('reset_password').'?error=invalid-token');
        }

        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            $passwordUpdater->hashPassword($user);
            $user->setResetToken(null);
            $user->setResetTokenExpirationDate(null);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('reset_password').'?success=1');
        }

        return $this->render('security/reset-password.html.twig', [
            'error' => $error,
            'changePasswordForm' => $changePasswordForm->createView(),
        ]);
    }
}
