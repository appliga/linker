<?php

namespace App\Controller;

use App\Entity\User;
use App\Filters\UserFilter;
use App\Form\ChangePasswordType;
use App\Form\EditUserType;
use App\Form\MyProfileType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\PasswordUpdater;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/admin/")
 */
class UserController extends AbstractController
{
    /**
     * @Route("users", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => array_reverse($userRepository->findAll()),
        ]);
    }

    /**
     * @Route("users-filters", name="user_filter", methods={"POST"}, options={"expose"=true})
     */
    public function filter(Request $request, UserRepository $userRepository, Security $security): Response
    {
        $userFilters = UserFilter::createByRequest($request, $security);

        $users = $userRepository->filter($userFilters)->getItems();

        return new JsonResponse(['users' => array_map(function (User $user) {
            return $user->toArray();
        }, $users)]);
    }

    /**
     * @Route("users/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, PasswordUpdater $passwordUpdater): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setAvatarBackground(User::AVATAR_BACKGROUNDS[array_rand(User::AVATAR_BACKGROUNDS)]);
            $passwordUpdater->hashPassword($user);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("users/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("users/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, PasswordUpdater $passwordUpdater): Response
    {
        $form = $this->createForm(EditUserType::class, $user);
        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);
        $changePasswordForm->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$user->getAvatarBackground()) {
                $user->setAvatarBackground(User::AVATAR_BACKGROUNDS[array_rand(User::AVATAR_BACKGROUNDS)]);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            $passwordUpdater->hashPassword($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'changePasswordForm' => $changePasswordForm->createView(),
        ]);
    }

    /**
     * @Route("/my-profile", name="my_profile", methods={"GET","POST"})
     */
    public function myProfile(Request $request, PasswordUpdater $passwordUpdater): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(MyProfileType::class, $user);
        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);
        $changePasswordForm->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$user->getAvatarBackground()) {
                $user->setAvatarBackground(User::AVATAR_BACKGROUNDS[array_rand(User::AVATAR_BACKGROUNDS)]);
            }
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Your profile has been updated!'
            );

            return $this->redirectToRoute('my_profile');
        }

        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            $passwordUpdater->hashPassword($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Your password has been updated!'
            );

            return $this->redirectToRoute('my_profile');
        }

        return $this->render('user/my-profile.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'changePasswordForm' => $changePasswordForm->createView(),
        ]);
    }

//    /**
//     * @Route("/{id}", name="user_delete", methods={"DELETE"})
//     */
//    public function delete(Request $request, User $user): Response
//    {
//        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($user);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('user_index');
//    }
}
