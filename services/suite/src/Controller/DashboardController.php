<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('manage_dashboard');
    }

    /**
     * @Route("/admin/manage-dashboard", name="manage_dashboard")
     */
    public function manage(): Response
    {
        return $this->render('dashboard/manage.html.twig');
    }
}
