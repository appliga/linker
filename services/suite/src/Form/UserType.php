<?php

namespace App\Form;

use App\Entity\Country;
use App\Entity\Department;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email(),
                ],
            ])
            ->add('fullName', TextType::class, [
                'label' => 'Full Name',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('position', TextType::class, [
                'label' => 'Position*',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('reportsTo', EntityType::class, [
                'class' => User::class,
                'label' => 'Reports To*',
                'constraints' => [
                    new NotBlank(),
                ],
                'choice_label' => 'fullName',
            ])
            ->add('department', EntityType::class, [
                'class' => Department::class,
                'label' => 'Department*',
                'constraints' => [
                    new NotBlank(),
                ],
                'choice_label' => 'name',
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'label' => 'Country*',
                'constraints' => [
                    new NotBlank(),
                ],
                'choice_label' => 'name',
            ])
            ->add('active', CheckboxType::class, [
                'data' => true,
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => [
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => [
                        'autocomplete' => 'new-password',
                    ],
                ],
                'first_options' => ['label' => 'form.password'],
                'second_options' => ['label' => 'form.password_confirmation'],
                'invalid_message' => 'Passwords should be identical',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 8]),
                ],
            ])
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'User' => 'ROLE_USER',
                    'Admin' => 'ROLE_ADMIN',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
