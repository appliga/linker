<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load($manager)
    {
        $user = new User();
        $user->setEmail('brzoski.pawel@gmail.com');
        $user->setFullName('Pawel Brzoski');
        $user->setActive(1);
        $user->setRole('ROLE_ADMIN');
        $user->setPassword($this->encoder->encodePassword($user, 'pawel123!'));
        $manager->persist($user);

        $manager->flush();
    }
}
