package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)
const port = "8080"
func main() {
	http.HandleFunc("/", link)
	http.HandleFunc("/register", register)
	http.HandleFunc("/status", status)
	http.ListenAndServe(":"+port, nil)

	fmt.Println("Server running on port: "+port)
}

func link(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("index.html")

	variables := struct {
		Url string
	}{
		Url: "allegro.pl",
	}

	if err != nil { // if there is an error
		log.Print("template parsing error: ", err) // log it
	}
	err = t.Execute(w, variables) //execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {       // if there is an error
		log.Print("template executing error: ", err) //log it
	}
}

func register(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("index.html")

	variables := struct {
		Url string
	}{
		Url: "register.allegro.pl",
	}

	if err != nil { // if there is an error
		log.Print("template parsing error: ", err) // log it
	}
	err = t.Execute(w, variables) //execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {       // if there is an error
		log.Print("template executing error: ", err) //log it
	}
}

func status(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("index.html")

	variables := struct {
		Url string
	}{
		Url: "status.allegro.pl",
	}

	if err != nil { // if there is an error
		log.Print("template parsing error: ", err) // log it
	}
	err = t.Execute(w, variables) //execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {       // if there is an error
		log.Print("template executing error: ", err) //log it
	}
}