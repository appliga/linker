package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)
const port = "8080"
func main() {
	http.HandleFunc("/", link)
	http.HandleFunc("/ses", ses)
	http.ListenAndServe(":"+port, nil)

	fmt.Println("Server running on port: "+port)
}

func link(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("index.html")

	variables := struct {
		Url string
		Title string
	}{
		Url: "aws.com",
		Title: "AWS Hooks Receiver",
	}

	if err != nil { // if there is an error
		log.Print("template parsing error: ", err) // log it
	}
	err = t.Execute(w, variables) //execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {       // if there is an error
		log.Print("template executing error: ", err) //log it
	}
}

func ses(w http.ResponseWriter, r *http.Request) {

	t, err := template.ParseFiles("index.html")

	variables := struct {
		Url string
		Title string
	}{
		Url: "ses.aws.com",
		Title: "SES Hooks Endpoint",
	}

	if err != nil { // if there is an error
		log.Print("template parsing error: ", err) // log it
	}
	err = t.Execute(w, variables) //execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {       // if there is an error
		log.Print("template executing error: ", err) //log it
	}
}